import mongoose from 'mongoose';
import { CreateProducerParams, IProducerRepository } from '../persistence/producer.repository';

export default class ProducerService {
    private producerRepository: IProducerRepository;

    constructor(producerRepository: IProducerRepository) {
        this.producerRepository = producerRepository;
    }

    async createProducer(params: CreateProducerParams) {
        return await this.producerRepository.createProducer(params);
    }

    async findProducerById(id: mongoose.Types.ObjectId) {
        return await this.producerRepository.findProducerById(id);
    }
}