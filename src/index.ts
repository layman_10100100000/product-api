import 'dotenv/config'
import express from 'express'
import {createHandler} from 'graphql-http/lib/use/express'
import schema from './infra/http/graphql/schema'
import mongoose from 'mongoose'

async function bootstrap() {
  const app = express()

  const mongoUsername = process.env.MONGO_USERNAME
  const mongoPassword = process.env.MONGO_PASSWORD
  const mongoHost = process.env.MONGO_HOST
  const mongoDatabase = process.env.MONGO_DATABASE
  // in case of using hosted DB solution
  const dbUrl = process.env.DB_CONNECTION_STRING

  const mongoUri = dbUrl || `mongodb://${mongoUsername}:${mongoPassword}@${mongoHost}/${mongoDatabase}?authSource=admin`
  console.log('mongoUri: ',mongoUri);

  // MongoDB connection
  mongoose
    .connect(mongoUri)
    .then(() => console.log('DB connection established...'))
    .catch((err) => {
      console.log('Error establishing connection: ',err)
      process.exit(1)
    })

  // GraphQL endpoint
  app.all('/graphql', createHandler({schema}))

  const PORT = process.env.PORT || 4000
  app.listen(PORT, () => {console.log(`Server is running on port ${PORT}`)})
}

bootstrap().catch((err) => {
  console.log('Error starting application: ',err)
  process.exit(1)
})