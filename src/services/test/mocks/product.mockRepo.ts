import mongoose from 'mongoose';
import { CreateProductParams, IProductRepository } from '../../../persistence/product.repository';
import { IProducer } from '../../../infra/db/models/Producer';
import { IProduct } from '../../../infra/db/models/Product';

export class MockProductRepository implements IProductRepository {
    private producers: any[] = [];
    private products: any[] = [];

    async createProducts(params: CreateProductParams[]) {
        // Create a mock product with a new ObjectId and the provided params
        const mockProducts = params.map(product => ({ ...product, _id: '123' }));
        this.products.push(...mockProducts as IProduct[]);
        return Promise.resolve(mockProducts as IProduct[]);
    }

    async findProductById(id: mongoose.Types.ObjectId) {
        // Find a product in the mock array by id
        const foundProduct = this.products.find(product => product._id === id);
        return Promise.resolve(foundProduct || null);
    }

    async findProductsByProducerId(producerId: mongoose.Types.ObjectId) {
        // Find products in the mock array by producerId
        const foundProducts = this.products.filter(product => product.producerId === producerId);
        return Promise.resolve(foundProducts);
    }

    async updateProduct(id: mongoose.Types.ObjectId, params: CreateProductParams) {
        // Find a product in the mock array by id
        const foundProduct = this.products.find(product => product._id === id);
        if (!foundProduct) {
            return Promise.resolve(null);
        }

        // Update the product with the provided params
        const updatedProduct = { ...foundProduct, ...params };
        this.products = this.products.map(product => {
            if (product._id === id) {
                return updatedProduct;
            }
            return product;
        });

        return Promise.resolve(updatedProduct);
    }

    async deleteProducts(ids: mongoose.Types.ObjectId[]) {
        // Delete products from the mock array by id
        this.products = this.products.filter(product => !ids.includes(product._id));
        return Promise.resolve({ deletedCount: ids.length }) as any;
    }
}