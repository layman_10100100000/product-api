import { GraphQLObjectType, GraphQLID, GraphQLList, GraphQLBoolean } from 'graphql';
import {
  productResolver,
  productsByProducerResolver
} from '../resolvers/query';
import {
  createProductsResolver,
  updateProductResolver,
  deleteProductsResolver,
  createProducerResolver,
  synchronizeProductsResolver
} from '../resolvers/mutation';
import { ProductType, ProductInputType, ProducerInputType, ProducerType } from './types';

// Root Query
export const Query = new GraphQLObjectType({
  name: 'Query',
  fields: {
    product: {
      type: ProductType,
      args: { _id: { type: GraphQLID } },
      resolve: productResolver
    },
    productsByProducer: {
      type: new GraphQLList(ProductType),
      args: { producerId: { type: GraphQLID } },
      resolve: productsByProducerResolver
    }
  }
});

// Mutations
export const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    createProducts: {
      type: new GraphQLList(ProductType),
      args: { products: { type: new GraphQLList(ProductInputType) }, producer: { type: ProducerInputType } },
      resolve: createProductsResolver
    },
    createProducer: {
      type: ProducerType,
      args: { producer: { type: ProducerInputType } },
      resolve: createProducerResolver
    },
    updateProduct: {
      type: ProductType,
      args: {
        _id: { type: GraphQLID },
        product: { type: ProductInputType }
      },
      resolve: updateProductResolver
    },
    deleteProducts: {
      type: new GraphQLList(GraphQLID),
      args: { _ids: { type: new GraphQLList(GraphQLID) } },
      resolve: deleteProductsResolver
    },
    synchronizeProducts: {
      type: GraphQLBoolean,
      resolve: synchronizeProductsResolver
    }
  }
});


