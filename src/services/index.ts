import { producerRepository, productRepository } from '../persistence';
import ProducerService from './producer.service';
import ProductService from './product.service';

const producerService = new ProducerService(producerRepository);
const productService = new ProductService(productRepository, producerRepository);

export {
    producerService,
    productService
}