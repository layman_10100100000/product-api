import ProducerService from '../producer.service';
import { IProducerRepository } from '../../persistence/producer.repository';
import mongoose from 'mongoose';
import { MockProducerRepository } from './mocks/producer.mockRepo';

jest.mock('mongoose', () => {
    const originalModule = jest.requireActual('mongoose');

    return {
        ...originalModule,
        Types: {
            ...originalModule.Types,
            ObjectId: jest.fn().mockImplementation(() => '123')
        }
    };
});



describe('ProducerService', () => {
    let producerService: ProducerService;
    let mockProducerRepository: IProducerRepository;


    beforeEach(() => {
        mockProducerRepository = new MockProducerRepository();
        producerService = new ProducerService(mockProducerRepository);
        jest.spyOn(mockProducerRepository, 'createProducer');
        jest.spyOn(mockProducerRepository, 'findProducerById');
    });    

    describe('createProducer', () => {
        it('should create a producer', async () => {
            const producerParams = { name: 'Test Producer', country: 'Test Country', region: 'Test Region' };
            const mockProducer = { ...producerParams, _id: '123' };

            const result = await producerService.createProducer(producerParams);

            expect(mockProducerRepository.createProducer).toHaveBeenCalledTimes(1);
            expect(mockProducerRepository.createProducer).toHaveBeenCalledWith(producerParams);
            expect(result).toEqual(mockProducer);
        });
    });

    describe('findProducerById', () => {
        it('should find a producer by ID', async () => {
            const mockProducerId = '123' as unknown as mongoose.Types.ObjectId;
            const mockProducer = { name: 'Test Producer', country: 'Test Country', region: 'Test Region', _id: mockProducerId };

            await producerService.createProducer(mockProducer);
            const result = await producerService.findProducerById(mockProducerId);

            expect(mockProducerRepository.findProducerById).toHaveBeenCalledTimes(1);
            expect(mockProducerRepository.findProducerById).toHaveBeenCalledWith(mockProducerId);
            expect(result).toEqual(mockProducer);
        });
    });
});
