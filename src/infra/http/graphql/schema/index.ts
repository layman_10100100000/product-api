import { GraphQLSchema } from 'graphql';
import { Mutation, Query } from './schema';

export default new GraphQLSchema({ query: Query, mutation: Mutation });