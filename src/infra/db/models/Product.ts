import mongoose, { Schema, Document } from 'mongoose';
import { IProducer } from './Producer';

export interface IProduct extends Document {
  vintage: string;
  name: string;
  producerId: mongoose.Types.ObjectId;
  producer: IProducer;
}

const productSchema: Schema = new Schema({
  vintage: { type: String, required: true },
  name: { type: String, required: true },
  producerId: { type: mongoose.Schema.Types.ObjectId, ref: 'Producer', required: true },
  producer: { type: mongoose.Schema.Types.ObjectId, ref: 'Producer' }
});

export default mongoose.model<IProduct>('Product', productSchema);
