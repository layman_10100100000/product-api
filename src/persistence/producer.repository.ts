import mongoose from 'mongoose';
import { IProducer } from '../infra/db/models/Producer';

export type CreateProducerParams = {
    name: string;
    country?: string;
    region?: string;
};

export interface IProducerRepository { 
    createProducer(params: CreateProducerParams): Promise<IProducer>;
    findProducerById(id:  mongoose.Types.ObjectId): Promise<IProducer | null>;
    findProducerByName(name: string): Promise<IProducer | null>;
}
