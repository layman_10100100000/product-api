import Producer from '../infra/db/models/Producer';
import Product from '../infra/db/models/Product';
import ProducerMongoRepository from './implementations/producer.mongoRepository';
import ProductMongoRepository from './implementations/product.mongoRepository';

const producerRepository = new ProducerMongoRepository(Producer);
const productRepository = new ProductMongoRepository(Product);

export {
    producerRepository,
    productRepository
}