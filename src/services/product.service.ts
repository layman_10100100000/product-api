import mongoose from 'mongoose'
//import dotenv from 'dotenv'
import fetch from 'cross-fetch'
import { Readable } from 'stream';
import csvParser from 'csv-parser';
import {IProductRepository} from '../persistence/product.repository'
import {IProducerRepository} from '../persistence/producer.repository'
import Product, {IProduct} from '../infra/db/models/Product'
import Producer, {IProducer} from '../infra/db/models/Producer'

export default class ProductService {
  private productRepository: IProductRepository
  private producerRepository: IProducerRepository

  constructor(
    productRepository: IProductRepository,
    producerRepository: IProducerRepository
  ) {
    this.productRepository = productRepository
    this.producerRepository = producerRepository
  }

  async createProducts(params: {products: IProduct[]; producer?: IProducer}) {
    const {products, producer} = params;
    const createdProducts = [];

    // A variable to hold the producer document, either found or created
    let producerDocument;

    // Check and create the producer if it doesn't exist
    if (producer && producer.name) {
        // Ideally, you should check if a producer with the same details already exists
        // to avoid duplicates, before creating a new one
        const existingProducerDocument = await this.producerRepository.findProducerByName(producer.name);
        if (existingProducerDocument) {
            producerDocument = existingProducerDocument;
        } else {
            producerDocument = await this.producerRepository.createProducer(producer);          
        }
    }

    for (const product of products) {
        const {vintage, name, producerId} = product;

        if (producerId && !mongoose.Types.ObjectId.isValid(producerId)) {
            throw new Error(`Invalid producerId: ${producerId}`);
        }

        if (producerId) {
            // If producerId is provided, find the existing producer
            const existingProducerDocument = await this.producerRepository.findProducerById(producerId);
            if (!existingProducerDocument) {
                throw new Error(`Producer not found for ID: ${producerId}`);
            }

            const newProduct = {
                vintage,
                name,
                producerId: existingProducerDocument._id,
                producer: existingProducerDocument,
            };

            createdProducts.push(newProduct);
        } else {
            // If producerId is not provided, use the previously created or found producerDocument
            if (!producerDocument) {
                throw new Error('Producer information is missing');
            }

            const newProduct = {
                vintage,
                name,
                producerId: producerDocument._id,
                producer: producerDocument,
            };

            createdProducts.push(newProduct);
        }
    }

    return await this.productRepository.createProducts(createdProducts);
  }

  async findProductById(id: mongoose.Types.ObjectId) {
    return await this.productRepository.findProductById(id)
  }

  async findProductsByProducerId(producerId: mongoose.Types.ObjectId) {
    return await this.productRepository.findProductsByProducerId(producerId)
  }

  async updateProduct(id: mongoose.Types.ObjectId, params: IProduct) {
    return await this.productRepository.updateProduct(id, params)
  }

  async deleteProducts(ids: mongoose.Types.ObjectId[]) {
    return await this.productRepository.deleteProducts(ids)
  }

  async synchronizeProducts() {
    // TODO: Integrate with reposotory classes
    // After all this should be treated as a backgorund process
    // and used separately from the API
    try {
      const response = await fetch(process.env.SYNC_URL as string);

      if (!response.ok) {
        throw new Error('Failed to fetch the CSV file');
      }
  
      const stream = (response.body as unknown as Readable).pipe(csvParser());
  
      const batchSize = 100;
      let batch: any = [];
      let total = 0;
  
      stream.on('data', async (data: any) => {
        if (!data.Producer || data.Producer.trim() === '') {
          console.error('ProducerName is missing or empty. Skipping row.');
          stream.resume();
          return;
        }
        // Handle backpressure by pausing the stream
        stream.pause();
  
        // Before upserting a Product, 
        // we need to ensure the Producer exists in the database and get its ObjectId. 
        // If the producer doesn't exist, we'll need to create it.
        let producer = await Producer.findOne({ name: data.Producer });
        if (!producer) {
          // If producer does not exist, create a new one
          producer = new Producer({ name: data.Producer, country: data.Country, region: data.Region });
          await producer.save();
        }
  
        batch.push({
          updateOne: {
            filter: { vintage: data.Vintage, name: data['Product Name'], producer: producer._id },
            update: { vintage: data.Vintage, name: data['Product Name'], producerId: producer._id },
            upsert: true,
          },
        });
  
        // Perform upsert in batches
        if (batch.length >= batchSize) {
          console.log('writing rows: ', batch.length);
          console.log('total rows written: ', total += batch.length);
          //log verbose
          await Product.bulkWrite(batch);
          batch = [];
          stream.resume();
        } else {
          stream.resume();
        }
      });
  
      stream.on('end', async () => {
        // Handle any remaining batch
        if (batch.length > 0) {
          await Product.bulkWrite(batch);
        }
  
        console.log('CSV data processing completed');
      });
  
      stream.on('error', (error: any) => {
        console.error('Error processing CSV stream:', error);
      });
    } catch (error) {
      console.error('Error fetching or processing CSV file:', error);
    }
  
    return true;
  }
}
