import { GraphQLObjectType, GraphQLString, GraphQLNonNull, GraphQLID, GraphQLInputObjectType } from 'graphql';

export const ProducerType = new GraphQLObjectType({
  name: 'Producer',
  fields: () => ({
    _id: { type: new GraphQLNonNull(GraphQLID) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    country: { type: GraphQLString },
    region: { type: GraphQLString }
  })
});

export const ProductType = new GraphQLObjectType({
  name: 'Product',
  fields: () => ({
    _id: { type: new GraphQLNonNull(GraphQLID) },
    vintage: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    producerId: { type: new GraphQLNonNull(GraphQLID) },
    producer: { type: ProducerType }
  })
});

export const ProductInputType = new GraphQLInputObjectType({
    name: 'ProductInput',
    fields: {
      vintage: { type: new GraphQLNonNull(GraphQLString) },
      name: { type: new GraphQLNonNull(GraphQLString) },
      producerId: { type: GraphQLID }
    }
  });

  export const ProducerInputType = new GraphQLInputObjectType({
    name: 'ProducerInput',
    fields: {
      name: { type: new GraphQLNonNull(GraphQLString) },
      country: { type: GraphQLString },
      region: { type: GraphQLString }
    }
  });
  
