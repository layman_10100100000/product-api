import mongoose from 'mongoose';
import { productService } from '../../../../services';

export const productResolver = async (_: any, args: { _id: mongoose.Types.ObjectId }) => {
  return await productService.findProductById(args._id);
};

export const productsByProducerResolver = async (_: any, args: { producerId: mongoose.Types.ObjectId }) => {
  return await productService.findProductsByProducerId(args.producerId);
};
