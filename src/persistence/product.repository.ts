import mongoose from 'mongoose';
import { IProduct } from '../infra/db/models/Product';
import { DeleteResult } from 'mongodb';

export type CreateProductParams = {
    vintage: string;
    name: string;
    producerId: mongoose.Types.ObjectId
};

export interface IProductRepository {
    createProducts(params: CreateProductParams[]): Promise<IProduct[]>;
    findProductById(id:  mongoose.Types.ObjectId): Promise<IProduct | null>;
    findProductsByProducerId(producerId: mongoose.Types.ObjectId): Promise<IProduct[]>;
    updateProduct(id: mongoose.Types.ObjectId, params: CreateProductParams): Promise<IProduct | null>;
    deleteProducts(ids: mongoose.Types.ObjectId[]): Promise<DeleteResult>;
}