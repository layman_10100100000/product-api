import mongoose from 'mongoose';
import Product, { IProduct } from '../../infra/db/models/Product';
import { CreateProductParams, IProductRepository } from '../product.repository';
import { DeleteResult } from 'mongodb';

type Product = typeof Product;


export default class ProductMongoRepository implements IProductRepository {
    private productModel: Product

    constructor(model: Product) {
        this.productModel = model;
    }

    async createProducts(products: CreateProductParams[]): Promise<IProduct[]> {
        return await this.productModel.insertMany(products)
    }

    async findProductById(id: mongoose.Types.ObjectId): Promise<IProduct | null> {
        return await this.productModel.findById(id).populate('producer');
    }

    async findProductsByProducerId(producerId: mongoose.Types.ObjectId): Promise<IProduct[]> {
        return await this.productModel.find({ producerId }).populate('producer');
    }

    async updateProduct(id: mongoose.Types.ObjectId, product: CreateProductParams): Promise<IProduct | null> {
        return await this.productModel.findByIdAndUpdate(id, product, { new: true });
    }

    async deleteProducts(ids: mongoose.Types.ObjectId[]): Promise<DeleteResult> {
        console.log('ids for deletion', ids);
        return await this.productModel.deleteMany({ _id: { $in: ids } });
    }
}