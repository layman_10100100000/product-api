module.exports = {
    roots: [
      './src',
    ],
    transform: {
      '^.+\\.(t|j)sx?$': ['@swc/jest'],
    },
    testEnvironment: 'node',
    // Worakround for node-fecth using EMS modules
    // https://github.com/node-fetch/node-fetch/issues/1265#issuecomment-977520130
    transformIgnorePatterns: [
      'node_modules/(?!(node-fetch|@swc/jest)/)',
    ],
  }