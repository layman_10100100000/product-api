import mongoose from 'mongoose';
import csvParser from 'csv-parser';
import fetch from 'cross-fetch';
import { Readable } from 'stream';
import Product, { IProduct } from '../../../db/models/Product';
import Producer, { IProducer } from '../../../db/models/Producer';
import { producerService, productService } from '../../../../services';


export const createProductsResolver = async (_: any, args: { products: IProduct[], producer?: IProducer }) => {
  return await productService.createProducts(args)
};

export const createProducerResolver = async (_: any, args: { producer: IProducer }) => {
  const { producer } = args;
  return await producerService.createProducer(producer);
}

export const updateProductResolver = async (_: any, args: { _id: mongoose.Types.ObjectId; product: IProduct }) => {
  return await productService.updateProduct(args._id, args.product);
};

export const deleteProductsResolver = async (_: any, args: { _ids: mongoose.Types.ObjectId[] }) => {
  await productService.deleteProducts(args._ids);
  return args._ids;
};

export const synchronizeProductsResolver = async () => {
  return await productService.synchronizeProducts();

    //const response = await fetch('https://api.frw.co.uk/feeds/all_listings.csv');

  //   if (!response.ok) {
  //     throw new Error('Failed to fetch the CSV file');
  //   }

  //   const stream = (response.body as unknown as Readable).pipe(csvParser());

  //   const batchSize = 100;
  //   let batch: any = [];
  //   let total = 0;

  //   stream.on('data', async (data: any) => {
  //     if (!data.Producer || data.Producer.trim() === '') {
  //       console.error('ProducerName is missing or empty. Skipping row.');
  //       stream.resume();
  //       return;
  //     }
  //     // Handle backpressure by pausing the stream
  //     stream.pause();

  //     // Before upserting a Product, 
  //     // you need to ensure the Producer exists in the database and get its ObjectId. 
  //     // If the producer doesn't exist, you'll need to create it.
  //     let producer = await Producer.findOne({ name: data.Producer });
  //     if (!producer) {
  //       // If producer does not exist, create a new one
  //       producer = new Producer({ name: data.Producer, country: data.Country, region: data.Region });
  //       await producer.save();
  //     }

  //     batch.push({
  //       updateOne: {
  //         filter: { vintage: data.Vintage, name: data['Product Name'], producer: producer._id },
  //         update: { vintage: data.Vintage, name: data['Product Name'], producerId: producer._id },
  //         upsert: true,
  //       },
  //     });

  //     // Perform upsert in batches
  //     if (batch.length >= batchSize) {
  //       console.log('writing rows: ', batch.length);
  //       console.log('total rows written: ', total += batch.length);
  //       //log verbose
  //       await Product.bulkWrite(batch);
  //       batch = [];
  //       stream.resume();
  //     } else {
  //       stream.resume();
  //     }
  //   });

  //   stream.on('end', async () => {
  //     // Handle any remaining batch
  //     if (batch.length > 0) {
  //       await Product.bulkWrite(batch);
  //     }

  //     console.log('CSV data processing completed');
  //   });

  //   stream.on('error', (error: any) => {
  //     console.error('Error processing CSV stream:', error);
  //   });
  // } catch (error) {
  //   console.error('Error fetching or processing CSV file:', error);
  // }

  // return true;
}
