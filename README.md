# Product API
 
 Simple Product API for managing Products and Producer entities

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [Directory structure](#contributing)
- [Future improvements and considerations](#improvements)

## Installation

Make sure you have Docker installed on your machine and running.
Copy contents from ```.env.example``` to ```.env```
Run ```docker compose up --build``` and the server should be up and running.

## Usage

Import the ```gql_product_api.postman_collection.json``` to your Postman and start testing the API.

## Directory structure

Directory structure:
```
.
├── Dockerfile
├── README.md
├── docker-compose.yml
├── gql_product_api.postman_collection.json
├── src
   ├──infra # Infra layer, contains the database and the GQL schema and resolvers
   ├──persistence # Persistency layer, contains the repositories
   ├──services # Service layer, contains the business logic

```

## Future improvements and considerations

- Adding more tests.
- Adding a production version. Currently it is only running the dev server using ```ts-node```.
- ```synchronizeProducts``` from ```productService``` is calling mongoose models directly. This should be done through the repositories.

