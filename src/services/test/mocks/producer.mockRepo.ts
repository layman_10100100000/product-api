import mongoose from 'mongoose';
import { CreateProducerParams, IProducerRepository } from '../../../persistence/producer.repository';
import { IProducer } from '../../../infra/db/models/Producer';

export class MockProducerRepository implements IProducerRepository {
    private producers: IProducer[] = [];

    async createProducer(params: CreateProducerParams) {
        // Create a mock producer with a new ObjectId and the provided params
        const mockProducer = { ...params, _id: '123' };

        this.producers.push(mockProducer as IProducer);
        return Promise.resolve(mockProducer as IProducer);
    }

    async findProducerById(id: mongoose.Types.ObjectId) {
        // Find a producer in the mock array by id
        const foundProducer = this.producers.find(producer => producer._id === id);
        return Promise.resolve(foundProducer || null);
    }
}