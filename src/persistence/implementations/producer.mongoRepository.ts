import mongoose from 'mongoose';
import Producer, { IProducer } from '../../infra/db/models/Producer';
import { IProducerRepository } from '../producer.repository';

type Producer = typeof Producer;

type CreateProducerParams = {
    name: string;
    country?: string;
    region?: string;
};

export default class ProducerMongoRepository implements IProducerRepository {
    private producerModel: Producer

    constructor(model: Producer) {
        this.producerModel = model;
    }

    async createProducer(params: CreateProducerParams): Promise<IProducer> {
        
        const { name, country, region } = params;
        
        if (!name) {
            throw new Error('Producer name is required');

        }
        
        return await this.producerModel.create({ name, country, region });
    }

    async findProducerById(id: mongoose.Types.ObjectId): Promise<IProducer | null> {
        return await this.producerModel.findById(id);
    }

    async findProducerByName(name: string): Promise<IProducer | null> {
        return await this.producerModel.findOne({ name });
    }
}