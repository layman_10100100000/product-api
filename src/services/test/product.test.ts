import mongoose from 'mongoose';
import ProductService from '../product.service';
import { IProductRepository } from '../../persistence/product.repository';
import { IProducerRepository } from '../../persistence/producer.repository';
import { MockProductRepository } from './mocks/product.mockRepo';
import { MockProducerRepository } from './mocks/producer.mockRepo';
import { IProduct } from '../../infra/db/models/Product';

jest.mock('mongoose', () => {
    const originalMongoose = jest.requireActual('mongoose');

    // Create a partial mock of ObjectId
    function MockObjectId() {
        return 'your-predictable-id'; // Return a predictable value
    }

    // Mock the isValid function to always return true
    // I dont need to validate the id here, just test the logic
    MockObjectId.isValid = () => true;

    return {
        ...originalMongoose,
        Types: {
            ...originalMongoose.Types,
            ObjectId: MockObjectId
        }
    };
});

describe('ProductService', () => {
    let productService: ProductService;
    let mockProductRepository: IProductRepository;
    let mockProducerRepository: IProducerRepository;

    beforeEach(() => {
        mockProductRepository = new MockProductRepository();
        mockProducerRepository = new MockProducerRepository();
        productService = new ProductService(mockProductRepository, mockProducerRepository);

        jest.spyOn(mockProductRepository, 'createProducts');
        jest.spyOn(mockProductRepository, 'findProductById');
        jest.spyOn(mockProductRepository, 'updateProduct');
        jest.spyOn(mockProductRepository, 'deleteProducts');
    });    

    // Create a mock producer to use in tests
    const mockProducer = { name: 'Test Producer', country: 'Test Country', region: 'Test Region' };
    const mockProductsWithProducerId = [
        { name: 'Test Product 1', vintage: 'Test vintage', producerId: '123' },
        { name: 'Test Product 2', vintage: 'Test vintage', producerId: '456' },
    ];

    const mockProductsWithoutProducerId = [
        { name: 'Test Product 1', vintage: 'Test vintage' },
        { name: 'Test Product 2', vintage: 'Test vintage' },
    ];

    describe('createProducts', () => {
        it('should throw an error if producer id is not found', async () => {
            try {
                const result = await productService.createProducts({products: mockProductsWithProducerId as any, producer: mockProducer as any});
            } catch (error: any) {
                expect(error.message).toEqual('Producer not found for ID: 456');
            }
        });

        it('should call create products repo', async () => {
            const mockProducerDocument = { ...mockProducer, _id: '123' };
            jest.spyOn(mockProducerRepository, 'findProducerById').mockResolvedValueOnce(mockProducerDocument as any);

            const result = await productService.createProducts({products: mockProductsWithoutProducerId as any, producer: mockProducer as any});

            expect(mockProducerRepository.findProducerById).toHaveBeenCalledTimes(0);
            expect(mockProductRepository.createProducts).toHaveBeenCalledTimes(1);
        });
    });

});